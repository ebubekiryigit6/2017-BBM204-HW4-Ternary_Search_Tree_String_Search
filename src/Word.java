import java.io.File;

/**
 * Created by Ebubekir
 */
public class Word {

    private int id;         // unique id for file (can be same for different files)
    private File file;      // reads from which file
    private String word;    // a word

    public Word(int id, File file, String word){
        this.id = id;
        this.file = file;
        this.word = word;
    }

    public int getId() {
        return id;
    }

    public File getFile() {
        return file;
    }

    public String getWord() {
        return word;
    }
}

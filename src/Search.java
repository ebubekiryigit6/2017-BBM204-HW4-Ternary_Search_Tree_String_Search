import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ebubekir.
 */
public class Search {

    public static final int WORD_SEARCH = 0;
    public static final int PHRASE_SEARCH = 1;

    /**
     * Searches Ternary Search Tree for a query or phrase.
     * If query is one word TYPE is 0
     * If query is a phrase TYPE is 1
     *
     * @param tst       TST to search
     * @param wordList  seperated query list
     * @param TYPE      search TYPE
     */
    public void search(TST tst, ArrayList<String> wordList, int TYPE){
        if (TYPE == WORD_SEARCH){
            wordSearch(tst,wordList);
        }
        else if (TYPE == PHRASE_SEARCH){
            phraseSearch(tst,wordList);
        }
        else {
            System.out.println("Invalid Search Type!");
        }
    }

    /**
     * Searches a word in Ternary Search Tree
     *
     * @param tst       TST to search
     * @param wordList  seperated query list
     */
    private void wordSearch(TST tst, ArrayList<String> wordList){
        if (wordList != null && tst != null)  {
            for (String word : wordList) {
                // search all keys one by one
                searchOneKey(tst, word);
            }
        }
    }

    /**
     * Searches a phrase in Ternary Search Tree
     *
     * @param tst       TST to search
     * @param wordList  seperated query list
     */
    private void phraseSearch(TST tst, ArrayList<String> wordList){
        if (wordList != null && tst != null){
            // query is a word, search this like a one word
            if (wordList.size() == 1){
                searchOneKey(tst,wordList.get(0));
            }
            else {
                // search all words and add hash map
                HashMap<String, ArrayList<Word>> keysMap = searchPhraseKeys(tst, wordList);
                // if only one return value is null, there is no founded items
                if (controlNullity(keysMap, wordList)) {
                    System.out.println("No result found:" + completeSearchKey(wordList));
                }
                else {
                    int foundKeyCount = 0;
                    // get first word
                    ArrayList<Word> firstList = keysMap.get(wordList.get(0));
                    // compare all founded word for same file
                    for (Word word : firstList) {
                        int totalCompare = 0;
                        int count = 1;
                        for (int i = 1; i < wordList.size(); i++) {
                            ArrayList<Word> foundList = keysMap.get(wordList.get(i));
                            // compare other founded list
                            int compare = compareItems(word,foundList,count);
                            if (compare == 0){
                                totalCompare = -1;
                                break;
                            }
                            else {
                                // compare successful
                                totalCompare = compare;
                                count++;
                            }
                        }
                        if (totalCompare == -1){
                            //System.out.println("No result found:" + completeSearchKey(wordList));
                        }
                        // phrase found!
                        else if (totalCompare == 1){
                            foundKeyCount++;
                            System.out.println(word.getFile().getName() + ";" + completeSearchKey(wordList) + ";" + word.getId());
                        }
                    }
                    if (foundKeyCount == 0){
                        // no result found
                        System.out.println("No result found:" + completeSearchKey(wordList));
                    }
                }
            }
        }
    }

    /**
     * Searches one key in TST and prints founded key's words, indexes, files..
     *
     * @param tst       TST to search
     * @param searchKey search Key
     */
    private void searchOneKey(TST tst,String searchKey){
        ArrayList<Word> foundedList = tst.contains(searchKey.toLowerCase());

        // no items found
        if (foundedList != null) {
            int count = 0;
            for (int i = 0; i < foundedList.size(); i++) {
                Word word = foundedList.get(i);
                if (word.getWord().equalsIgnoreCase(searchKey)) {
                    // print founded items
                    System.out.println(word.getFile().getName() + ";" + searchKey + ";" + word.getId());
                } else {
                    count++;
                }
            }

            if (count == foundedList.size()) {
                System.out.println("No result found:" + searchKey);
            }
        } else {
            System.out.println("No result found:" + searchKey);
        }
    }

    /**
     * Indexes all keys to a map.
     *
     * @param tst       TST to search
     * @param wordList  seperated query list
     * @return          keys-founded words map
     */
    private HashMap<String,ArrayList<Word>> searchPhraseKeys(TST tst, ArrayList<String> wordList){
        HashMap<String,ArrayList<Word>> keysMap = new HashMap<>();
        for (String word : wordList) {
            ArrayList<Word> foundList = tst.contains(word.toLowerCase());
            // list can be null
            keysMap.put(word,foundList);
        }
        return keysMap;
    }

    /**
     * Controls all founded word list's nullity.
     *
     * @param keysMap   query-foundList map
     * @param wordList  seperated query list
     * @return          if list is null return true else false
     */
    private boolean controlNullity(HashMap<String,ArrayList<Word>> keysMap, ArrayList<String> wordList){
        boolean nullity = false;
        if (wordList.size() != keysMap.size()){
            return true;
        }
        for (int i = 0; i < keysMap.size(); i++){
            if (keysMap.get(wordList.get(i)) == null){
                // null item found
                nullity = true;
                break;
            }
        }
        return nullity;
    }

    /**
     * Combines all string to one string.
     *
     * @param wordList  seperated query list
     * @return          combined seperated query list
     */
    private String completeSearchKey(ArrayList<String> wordList){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < wordList.size(); i++){
            if (i == 0){
                stringBuilder.append(wordList.get(i));
            }
            else {
                stringBuilder.append(" ");
                stringBuilder.append(wordList.get(i));
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Compares a Word and array of Word
     * It means same files and consecutive id's
     *
     * @param word          a word object
     * @param foundedList   founded word list according to the query
     * @param count         the degree of proximity
     * @return              if word's file equals other word's file and id's consecutive
     */
    private int compareItems(Word word, ArrayList<Word> foundedList, int count) {
        int result = 0;
        for (Word other : foundedList) {
            if (other.getFile().getPath().equals(word.getFile().getPath())) {
                if (other.getId() == word.getId() + count) {
                    result = 1;
                    break;
                }
            }
        }
        return result;
    }
}

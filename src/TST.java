import java.util.ArrayList;

/**
 * Created by Ebubekir.
 */
public class TST {

    private Node root;   // creates a root node which contains \u0000 char data

    public TST(){
        this.root = new Node();
    }

    /**
     * TST insert function, insert function calls recursive insert function
     * Adds all nodes to root node
     *
     * @param word string to add
     * @return returns a boolean value, this value gives process' successfully.
     */
    public boolean insert(Word word){
        if (word == null || word.getWord().equals("")){
            throw new IllegalArgumentException("Search query is null!");
        }
        else {
            insert(root,word,0);
            return true;
        }
    }

    /**
     * Searches a word in TST.
     * If search successful returns a array of Word objects.
     * If search miss returns null
     *
     * @param word     search query
     * @return         array of founded Word objects
     */
    public ArrayList<Word> contains(String word){
        if (word == null || word == null || word.equals("")){
            throw new IllegalArgumentException("Search query is null!");
        }
        else {
            return contains(root,word,0);
        }
    }

    /**
     * Recursively added all nodes to root node.
     *
     * @param node  node to add
     * @param word  added Word object
     * @param index string index
     * @return added node
     */
    private Node insert(Node node, Word word, int index){
        char data = word.getWord().charAt(index);
        char lowerData = Character.toLowerCase(data);
        // free node
        if (node == null){
            node = new Node(lowerData);
        }
        // less according to ASCII
        if (lowerData < node.getData()){
            node.left = insert(node.left,word,index);
        }
        // greater according to ASCII
        else if (lowerData > node.getData()){
            node.right = insert(node.right,word,index);
        }
        // is word's end
        else if (index < word.getWord().length() - 1){
            node.middle = insert(node.middle,word,index + 1);
        }
        // equal, add object here
        else {
            node.endOfString = true;
            node.wordList.add(word);
        }
        return node;
    }

    /**
     * Searches a word in TST.
     * If search successful returns a array of Word objects.
     * If search miss returns null
     *
     * @param node      node to add
     * @param word      search query
     * @param index     word's char index
     * @return          array of founded Word objects
     */
    private ArrayList<Word> contains(Node node, String word, int index){
        // empty node
        if (node == null){
            return null;
        }
        char data = word.charAt(index);
        // less according to ASCII table
        if (data < node.getData()){
            return contains(node.left, word, index);
        }
        // greater than node's data
        else if (data > node.getData()){
            return contains(node.right, word, index);
        }
        // is word's end?
        else if (index < word.length() - 1){
            return contains(node.middle,word,index + 1);
        }
        // founded return node's Words list
        else {
            return node.wordList;
        }
    }


}

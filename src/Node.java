import java.util.ArrayList;

/**
 * Created by Ebubekir.
 */
public class Node {

    public Node left;                   // left pointer
    public Node right;                  // right pointer
    public Node middle;                 // mid pointer
    private char data;                  // character data
    public ArrayList<Word> wordList;    // end of word's list
    public boolean endOfString;         // end of any string?


    public Node(char data){
        this.data = data;
        this.wordList = new ArrayList<>();
    }

    public Node(){}

    public char getData() {
        return data;
    }
}

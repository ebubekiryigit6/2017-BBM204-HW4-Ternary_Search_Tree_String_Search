import java.io.*;
import java.util.ArrayList;

/**
 * Created by Ebubekir.
 */
public class FileUtils {

    private File rootDir;               // given path
    private ArrayList<File> fileList;   // indexed files

    public FileUtils(String rootDirPath){
        this.rootDir = new File(rootDirPath);
        this.fileList = indexFiles(rootDir);
    }

    /**
     * Recursively index all files in directories. If argument is a file no problem add file to array.
     *
     * @param file file that we want to index it
     * @return indexed file list
     */
    private ArrayList<File> indexFiles(File file){
        ArrayList<File> files = new ArrayList<>();
        // open directory
        if (file.isDirectory()) {
            findFiles(files, file);
        }
        // add array
        else if (file.isFile()){
            files.add(file);
        }
        // invalid file format
        else {
            System.out.println("This is not a directory or file!");
        }
        return files;
    }

    /**
     * Recursively finds all file in a directory.
     *
     * @param fileList add founded files to list
     * @param dir directory
     */
    private void findFiles(ArrayList<File> fileList, File dir){
        File[] files = dir.listFiles();
        for (File temp: files) {
            if (temp.isDirectory()){
                findFiles(fileList,temp);
            }
            else if (temp.isFile()){
                fileList.add(temp);
                System.out.println(temp.getName() + " indexed");
            }
        }
    }

    /**
     * Reads a file and finds all words in file.
     * And indexes all words, add all words a Ternary Search Tree.
     *
     * @param tst       adds indexed word to Ternary Search Tree
     * @param file      file to read
     */
    private void readFile(TST tst, File file){

        // file's lines
        StringBuilder stringBuilder = new StringBuilder();

        try {
            String str;
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

            while ((str = in.readLine()) != null) {
                // replace delimiters
                str = str.replace('\n',' ').replace('\t',' ').replace(',',' ').trim();
                stringBuilder.append(str);
                stringBuilder.append(" ");
            }
            in.close();

            // splits file to words
            seperateWords(stringBuilder,file,tst);

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Split the string builder and insert all words to Ternary Search Tree
     *
     * @param stringBuilder file's lines
     * @param file          indexed file
     * @param tst           Global Ternary Search Tree
     */
    private void seperateWords(StringBuilder stringBuilder, File file, TST tst){
        String[] splitted = stringBuilder.toString().trim().split("\\s+");
        for (int i = 0; i < splitted.length; i++){
            tst.insert(new Word(i+1,file,splitted[i]));
        }
    }

    /**
     * Reads all files and index all words
     *
     * @param tst   global TST
     */
    public void indexAllWords(TST tst){
        for (int i = 0; i < fileList.size(); i++){
            readFile(tst,fileList.get(i));
        }
    }
}

/**
 *
 * Author:      Ebubekir
 *
 * File:        Exp4.java
 *
 * Purpose:     TERNARY SEARCH TREE implementation
 *
 *              Finds and reads all files in a given folder.
 *              It then stores the words in a Ternary Search Tree.
 *              When a search command arrives, it can quickly search for a word or a phrase.
 *
 *
 *
 * Compile:     javac Exp4.java
 *
 *
 * Run:         java Exp4 <directory path>
 *
 *
 * Input:       A search command in command line.
 *
 *              Search format is:
 *                                  search -w <word group>  -->  searches word group one by one
 *                                  search -W <word group>  -->  searches only word group
 *
 *
 *
 * Output:      The output shows which file in which the searched
 *              query is located and how many indexes the file is in.
 *
 *              Example:      search -w Mustafa Kemal Atatürk
 *                            File1; Mustafa; 1
 *                            File1; Kemal; 1
 *                            File1; Atatürk; 1
 *                            File3; Atatürk; 3
 *                            .
 *                            .
 *                            .
 *
 *
 * Notes:       Application has no CASE SENSITIVE !
 *
 *
 *
 */


import java.io.InputStreamReader;
import java.io.SyncFailedException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Exp4 {

    public static void main(String[] args) {

        // new ternary search tree
        TST tst = new TST();

        // open new directory and index files
        FileUtils fileUtils = new FileUtils(args[0]);

        // read all files and add all words to Ternary Search Tree
        fileUtils.indexAllWords(tst);

        while (true) {

            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine().trim();

            // quit command
            if (line.equalsIgnoreCase("q")){
                break;
            }

            String[] options = line.split("\\s+");
            // input must be like search -w <query>
            if (options.length < 3){
                System.out.println("Invalid Search Parameters! Command should be \"search -w/-W <query>\"");
            }
            else if (options[0].equalsIgnoreCase("search")){
                ArrayList<String> parameters = new ArrayList<>(Arrays.asList(Arrays.copyOfRange(options,2,options.length)));

                // create a new search process
                Search search = new Search();
                switch (options[1]) {
                    case "-w":
                        search.search(tst, parameters, Search.WORD_SEARCH);
                        break;
                    case "-W":
                        search.search(tst, parameters, Search.PHRASE_SEARCH);
                        break;
                    default:
                        System.out.println("Invalid Option Type! Options must be -w or -W");
                        break;
                }
            }
            else {
                System.out.println("Application supports only \"search\" command!");
            }
        }
    }


}
